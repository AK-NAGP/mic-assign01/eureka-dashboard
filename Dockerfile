FROM akshatjain244/base-spring-image:latest AS build
MAINTAINER akshat.jain01@nagarro.com

WORKDIR /home/app
ADD pom.xml /home/app
# RUN mvn verify clean --fail-never

COPY . /home/app
#RUN apt-get update
RUN mvn -v
RUN mvn clean install -DskipTests

FROM openjdk:8-jdk-alpine
RUN apk --no-cache add curl
COPY --from=build /home/app/target/eureka-dashboard-0.0.1-SNAPSHOT.jar /home/app/eureka-dashboard-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/home/app/eureka-dashboard-0.0.1-SNAPSHOT.jar"]
EXPOSE 8761
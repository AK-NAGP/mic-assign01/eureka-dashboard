--USER_MANAGEMENT DB
CREATE SCHEMA IF NOT EXISTS uc_ums;
CREATE USER uc_ums PASSWORD 'uc_ums';
GRANT ALL ON SCHEMA uc_ums TO uc_ums;
ALTER USER uc_ums ADMIN FALSE;

--SERVICE CATALOG DB
CREATE SCHEMA IF NOT EXISTS uc_service_catalog;
CREATE USER uc_service_catalog PASSWORD 'uc_service_catalog';
GRANT ALL ON SCHEMA uc_service_catalog TO uc_service_catalog;
ALTER USER uc_service_catalog ADMIN FALSE;

--SERVICE ORDERS DB
CREATE SCHEMA IF NOT EXISTS uc_orders;
CREATE USER uc_orders PASSWORD 'uc_orders';
GRANT ALL ON SCHEMA uc_orders TO uc_orders;
ALTER USER uc_orders ADMIN FALSE;

--SERVICE PAYMENTS DB
CREATE SCHEMA IF NOT EXISTS uc_payments;
CREATE USER uc_payments PASSWORD 'uc_payments';
GRANT ALL ON SCHEMA uc_payments TO uc_payments;
ALTER USER uc_payments ADMIN FALSE;

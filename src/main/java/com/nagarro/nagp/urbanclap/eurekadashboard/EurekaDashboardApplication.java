package com.nagarro.nagp.urbanclap.eurekadashboard;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

@SpringBootApplication
@EnableEurekaServer
@EnableHystrixDashboard
public class EurekaDashboardApplication {

	@Value("${spring.h2.remote.port:8762}")
	private String dbPort;

	public static void main(String[] args) {
		SpringApplication.run(EurekaDashboardApplication.class, args);
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server inMemoryH2DatabaseServer() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", dbPort);
	}
}
